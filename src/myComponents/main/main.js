import React, { Component } from 'react'
import Article from '../../myComponents/l_article/article'
import Section from '../../myComponents/r_section/section'


class Main extends Component {
    render() {
        return (
            <div className="main">
                <Article/>
                <Section/>
            </div>
        );
    }
}

export default Main;