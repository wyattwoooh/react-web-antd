import React, { Component } from 'react'
import './footer.css'

import imgURL from "../../img/docschina-qr1.jpeg"
import logoURL from "../../img/cover.png"

class Footer extends Component {
    render() {
        return (
            <div className="footer">
                <div className="css_l_content">
                    <img src={logoURL} alt="印记中文 logo" style={{ maxWidth: 160, height: 'auto' , marginBottom: 20}} />
                    <p>Copyright © 2020 Facebook Inc. </p>
                    <p>印记中文</p>
                    <img src={imgURL} alt="css logo" />
                </div>
                <div className="css_r_content">
                    <div className="css_docs">
                        <p>文档</p>
                        <ul>
                            <li>安装</li>
                            <li>核心概念</li>
                            <li>高级指引</li>
                            <li>API</li>
                            <li>Hook</li>
                            <li>测试</li>
                            <li>Concurrent 模式</li>
                            <li>贡献</li>
                            <li>FQA</li>
                        </ul>
                    </div>
                    <div className="css_channels">
                        <p>CHANNELS</p>
                        <ul>
                            <li>GitHub</li>
                            <li>Stackoverflow</li>
                            <li>Discussion 论坛</li>
                            <li>Reactflux 聊天室</li>
                            <li>DEV 社区</li>
                            <li>Facebook</li>
                            <li>Twitter</li>
                        </ul>
                    </div>
                    <div className="css_community">
                        <p>COMMUNITY</p>
                        <ul>
                            <li>Code of Conduct</li>
                            <li>社区资源</li>
                    </ul>
                    </div>
                    <div className="css_others">
                        <p>其他</p>
                        <ul>
                            <li>教程</li>
                            <li>博客</li>
                            <li>致谢</li>
                            <li>React Native</li>
                            <li>Privacy</li>
                            <li>Terms</li>
                        </ul>
                    </div>
                </div>
            </div>
        );
    }
}

export default Footer;