import React, { Component } from 'react'
import './article.css'

const  LiItems = [
        {
            id: 1,
            value: "体验 React",
            target: "#tryReact"
        },
        {
            id: 2,
            value: "学习 React",
            target: "#learnReact"
        },
        {
            id: 3,
            value: "获取最新消息",
            target: "#getNews"
        },
        {
            id: 4,
            value: "版本化文档",
            target: "#getDoc"
        },
        {
            id: 5,
            value: "找不到想要的？",
            target: "#somethingMissing"
        }
    ]

class Article extends Component {
    
    render() {
        return (
            <div className="article">
                <h1>开始</h1>
                <div className="css-react">
                    <p>这是一个 React 文档及相关资源的概览页面。</p>
                    <p>
                        <strong>React</strong>
                        是一个用于构建用户界面的 Javascript 库。你可以在首页或教程中学习什么是React。
                    </p>
                    <hr/>
                    <div className="css-ul">
                        <ul>
                            {
                                LiItems.map(item => {
                                    return (
                                        <li key={item.id}><a href={item.target}>{item.value}</a></li>
                                    )
                                })
                            }
                        </ul>
                    </div>
                    <hr/>
                    <div className="wrapper">
                        <h2 id="tryReact">体验 React</h2>
                        <p>
                            React 从诞生之初就是可被逐步采用的，因而
                            <strong>你可以按需引入或多或少的 React 特性</strong>
                            。不管你是想体验下 React，用它给简单的 HTML 页面增加一点交互，还是要开始一个完全由 React 驱动的复杂应用，该章节内容里的链接都能帮你快速开始。
                        </p>
                    </div>
                    <div className="wrapper">
                        <h2>在线体验</h2>
                        <p>
                        如果你对体验 React 感兴趣，可以尝试在线代码编辑器。从 CodePen，CodeSandbox，Glitch, 或者 Stackblitz 开始一个 React 版本的 Hello World 模版
                        </p>
                        <p>如果你喜欢使用自己的文本编辑器，也可以下载这个 HTML 文件，然后编辑文件内容，最后再用浏览器从本地文件系统打开文件，预览页面效果。注意：这个文件中包含一个低效率的运行时代码转换脚本，所以我们推荐仅在简单的演示项目中使用。</p>
                    </div>
                    <div className="wrapper">
                        <h2>在网站中添加 React</h2>
                        <p>
                        你可以立即在 HTML 文件中添加 React，然后选择逐渐拓展它的应用范围，或只在一些动态小部件中使用它。
                        </p>
                    </div>
                    <div className="wrapper">
                        <h2>创建新的 React应用</h2>
                        <p>
                        当你刚开始一个 React 应用时，通过 HTML 的 script 标签引入 React 依然是最好的选项，因为这能让你的项目立即启动。
                        </p>
                        <p>
                        但随着应用越来越大，你可能会需要更加集成化的安装方式。我们推荐了一些 JavaScript 工具链，它们适合大型应用。它们只需很少甚至零配置，就能让你充分利用丰富的 React 生态。立即尝试。
                        </p>
                    </div>
                    <hr/>
                    <div className="wrapper">
                        <h2 id="learnReact">学习 React</h2>
                        <p>
                            React 从诞生之初就是可被逐步采用的，因而
                            <strong>你可以按需引入或多或少的 React 特性</strong>
                            。不管你是想体验下 React，用它给简单的 HTML 页面增加一点交互，还是要开始一个完全由 React 驱动的复杂应用，该章节内容里的链接都能帮你快速开始。
                        </p>
                    </div>
                    <div className="wrapper">
                        <h2>第一个示例</h2>
                        <p>
                        如果你对体验 React 感兴趣，可以尝试在线代码编辑器。从 CodePen，CodeSandbox，Glitch, 或者 Stackblitz 开始一个 React 版本的 Hello World 模版
                        </p>
                        <p>如果你喜欢使用自己的文本编辑器，也可以下载这个 HTML 文件，然后编辑文件内容，最后再用浏览器从本地文件系统打开文件，预览页面效果。注意：这个文件中包含一个低效率的运行时代码转换脚本，所以我们推荐仅在简单的演示项目中使用。</p>
                    </div>
                    <div className="wrapper">
                        <h2>初学者的 React</h2>
                        <p>
                        你可以立即在 HTML 文件中添加 React，然后选择逐渐拓展它的应用范围，或只在一些动态小部件中使用它。
                        </p>
                    </div>
                    <div className="wrapper">
                        <h2>设计师的 React应用</h2>
                        <p>
                        当你刚开始一个 React 应用时，通过 HTML 的 script 标签引入 React 依然是最好的选项，因为这能让你的项目立即启动。
                        </p>
                        <p>
                        但随着应用越来越大，你可能会需要更加集成化的安装方式。我们推荐了一些 JavaScript 工具链，它们适合大型应用。它们只需很少甚至零配置，就能让你充分利用丰富的 React 生态。立即尝试。
                        </p>
                    </div>
                    <div className="wrapper">
                        <h2 id="getNews">获取最新消息</h2>
                        <p>
                            React 从诞生之初就是可被逐步采用的，因而
                            <strong>你可以按需引入或多或少的 React 特性</strong>
                            。不管你是想体验下 React，用它给简单的 HTML 页面增加一点交互，还是要开始一个完全由 React 驱动的复杂应用，该章节内容里的链接都能帮你快速开始。
                        </p>
                        <p>React 博客是 React 团队发布更新的官方渠道。一切重要的信息，包括：更新日志，废弃通知等，都会首先在这里发布</p>
                        <p>你也可以在 Twitter 上关注 @reactjs 账号获取更新，但即使不这样做，仅通过官方博客你也不会错过任何必要的信息。</p>
                    </div>
                    <div className="wrapper">
                        <p>并非每一个 React 版本都值得我们在博客上发布文章，但你可以在 React 代码仓库中的 CHANGELOG.md 文件或更新日志页面找到每个版本的更新日志。</p>
                    </div>
                    <div className="wrapper">
                        <h2 id="getDoc">版本化文档</h2>
                        <p>
                            React 从诞生之初就是可被逐步采用的，因而
                            <strong>你可以按需引入或多或少的 React 特性</strong>
                            。不管你是想体验下 React，用它给简单的 HTML 页面增加一点交互，还是要开始一个完全由 React 驱动的复杂应用，该章节内容里的链接都能帮你快速开始。
                        </p>
                    </div>
                    <hr/>
                    <div className="wrapper">
                        <h2 id="somthingMissing">找不到想要的？</h2>
                        <p>
                        如果你找不到想要的内容或觉得文档某些地方让人疑惑，请移步文档仓库提交 issue 或在 Twitter 上提及 @reactjs 账号。我们期待你的反馈！
                        </p>
                    </div>
                </div>
            </div>
        );
    }
}

export default Article;