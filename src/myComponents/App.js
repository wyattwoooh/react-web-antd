import React, { Component } from 'react'
import './App.css'
import Header from '../myComponents/header/header'
import Main from '../myComponents/main/main'
import Footer from '../myComponents/footer/footer'

class App extends Component {
    constructor(props) {
        super(props)
        this.state = {
            
        }


    }
    
    render() {
        return (
            <div className="root">
                <Header />
                <Main />
                <Footer />
            </div>
        );
    }
}

export default App;