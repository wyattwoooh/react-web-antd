import React, { Component } from 'react'
import './section.css'
import { Menu } from 'antd'

const { SubMenu } = Menu

const menuArr = [
    {
        key: "a1",
        value: "Hello World"
    },
    {
        key: "a2",
        value: "JSX 简介"
    },
    {
        key: "a3",
        value: "元素渲染"
    },
    {
        key: "a4",
        value: "组件 & Props"
    },
    {
        key: "a5",
        value: "State & 生命周期"
    },
    {
        key: "a6",
        value: "事件处理"
    },
    {
        key: "a7",
        value: "条件渲染"
    },
    {
        key: "a8",
        value: "列表 & Key"
    },
    {
        key: "a9",
        value: "表单"
    },
    {
        key: "a10",
        value: "状态提升"
    },
    {
        key: "a11",
        value: "组合 vs 继承"
    },
    {
        key: "a12",
        value: "React 哲学"
    }
]


class Section extends Component {
    render() {
        return (
            <div className="section">
                <Menu
                    style={{ width: 300, background: '#f7f7f7', border: 0 }}
                    defaultSelectedKeys={['1']}
                    mode="inline"
                >
                <SubMenu
                    key="sub1"
                    title={
                        <span>安装</span>
                    }
                    style={{ background: '#f7f7f7' }}
                >
                <Menu.Item key="1">开始</Menu.Item>
                <Menu.Item key="2">在网站中添加React</Menu.Item>
                <Menu.Item key="3">创建新的 React 项目</Menu.Item>
                <Menu.Item key="4">CDN 链接</Menu.Item>
                <Menu.Item key="5">发布渠道</Menu.Item>
                </SubMenu>
                <SubMenu key="sub2" title="核心概念">
                    {
                        menuArr.map(item => {
                            return (
                                <Menu.Item key={item.key}>{item.value}</Menu.Item>
                            )
                        })
                    }
                </SubMenu>
                <SubMenu
                    key="sub3"
                    title="API REFERENCE"
                >
                <Menu.Item key="8">React Component</Menu.Item>
                <Menu.Item key="9">ReactDom</Menu.Item>
                <Menu.Item key="10">DOM 元素</Menu.Item>
                <Menu.Item key="11">合成事件</Menu.Item>
                </SubMenu>
                <SubMenu key="sub5" title="高级指引">
                    <Menu.Item key="i1">无障碍</Menu.Item>
                    <Menu.Item key="i2">代码分割</Menu.Item>
                    <Menu.Item key="i3">Context</Menu.Item>
                </SubMenu>
                <SubMenu key="sub4" title="HOOK">
                    <Menu.Item key="h1">HOOK 简介</Menu.Item>
                    <Menu.Item key="h2">HOOK 概览</Menu.Item>
                </SubMenu>
                <SubMenu key="sub6" title="测试">
                    <Menu.Item key="t1">测试概览</Menu.Item>
                    <Menu.Item key="t2">测试技巧</Menu.Item>
                </SubMenu>
                <SubMenu key="sub7" title="CONCURRENT 模式">
                    <Menu.Item key="c1">介绍 concurrent 模式</Menu.Item>
                    <Menu.Item key="c2">Concurrent UI 模式</Menu.Item>
                </SubMenu>
                <SubMenu key="sub8" title="贡献">
                    <Menu.Item key="g1">如何参与</Menu.Item>
                    <Menu.Item key="g2">源码概述</Menu.Item>
                    <Menu.Item key="g3">设计理念</Menu.Item>
                </SubMenu>
                <SubMenu key="sub9" title="FQA">
                    <Menu.Item key="f1">AJAX 及 APIs</Menu.Item>
                    <Menu.Item key="f2">组件状态</Menu.Item>
                    <Menu.Item key="f3">在组件中使用事件处理函数</Menu.Item>
                    <Menu.Item key="f4">样式及CSS</Menu.Item>
                    <Menu.Item key="f5">项目文件结构</Menu.Item>
                </SubMenu>
            </Menu>
            </div>
        );
    }
}

export default Section;