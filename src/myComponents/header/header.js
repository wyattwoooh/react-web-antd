import React, { Component } from 'react'
import { Menu , Input} from 'antd'
import './header.css'

const svgURL = 'https://react.docschina.org/search.svg'


class Header extends Component {
    render() {
        return (
            <div className="header">
                <div className="mini_icon">
                    <img src={require("../../img/logo192.png")} alt="react icon" height="20" />
                    <p>React</p>
                </div>
                <Menu mode="horizontal" className="nav">
                    <Menu.Item key="doc">
                    文档
                    </Menu.Item>
                    <Menu.Item key="tutorial">
                    教程
                    </Menu.Item>
                    <Menu.Item key="blog">
                    博客
                    </Menu.Item>
                    <Menu.Item key="community">
                    社区
                    </Menu.Item>
                </Menu>
                <div className="search_box">
                    <Input placeholder="搜索" bordered={false} prefix={<img src={svgURL} alt="icon"/>}/>
                </div>
            </div>
        );
    }
}

export default Header;